package minesweeper.userinterface.graphic.field.scoreproccess.getusername;

import minesweeper.userinterface.graphic.field.controller.GraphicController;
import minesweeper.userinterface.graphic.NewWindowPanel;
import minesweeper.userinterface.graphic.ReturnAction;

import javax.swing.*;
import java.awt.*;

public final class GetNameMenu extends NewWindowPanel
{
    private static final int BORDER_WIDTH = 3;
    private static final int NAME_FIELD_LENGTH = 10;

    public GetNameMenu(GraphicController controller)
    {
        super(new BorderLayout());

        assert (null != controller);

        JLabel textLabel = new JLabel("Congratulations! Your name:");
        textLabel.setBorder(BorderFactory.createLineBorder(Color.WHITE, BORDER_WIDTH));
        add(textLabel, BorderLayout.NORTH);

        JTextField playerNameField = new JTextField(NAME_FIELD_LENGTH);
        add(playerNameField, BorderLayout.CENTER);

        JButton readyButton = new JButton("Ok");
        readyButton.addActionListener(new GetNameAction(playerNameField, controller));
        readyButton.addActionListener(new ReturnAction());
        add(readyButton, BorderLayout.SOUTH);
    }
}
