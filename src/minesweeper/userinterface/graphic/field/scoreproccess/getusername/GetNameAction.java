package minesweeper.userinterface.graphic.field.scoreproccess.getusername;

import minesweeper.userinterface.graphic.GoToMenuAction;
import minesweeper.userinterface.graphic.field.controller.GraphicController;
import minesweeper.userinterface.graphic.field.scoreproccess.ShowScoresMenu;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public final class GetNameAction implements ActionListener
{
    private JTextField textField;
    GraphicController controller;

    public GetNameAction(JTextField textField, GraphicController controller)
    {
        assert (null != textField);

        this.textField = textField;
        this.controller = controller;
    }

    public void actionPerformed(ActionEvent event)
    {
        controller.saveScore(textField.getText());
        GoToMenuAction.switchPanel(new ShowScoresMenu());
    }
}
