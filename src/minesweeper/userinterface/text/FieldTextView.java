package minesweeper.userinterface.text;

import minesweeper.model.cell.Cell;
import minesweeper.model.field.Field;

import minesweeper.userinterface.text.controller.FieldTextController;

public final class FieldTextView
{
    private int fieldWidth;
    private int fieldHeight;
    private Field fieldModel;
    private FieldTextController fieldController;

    private static final int MESSAGE_COUNT = 3;
    private static final String[] MESSAGES =
    {
        "Command list:\no <x> <y> - open cell(x,y);\n"
        + "c <x> <y> - set flag on cell(x,y);\n"
        + "n <width> <height> <mine count> - start new game with new parameters;\n"
        + "r - restart game;\ne - exit game;",
        "Incorrect command format, enter h for help.",
        "Data is incorrect. Every parameter should be positive,"
        + " and mine count should be less than cell count"                                  };

    private static final int MAX_DIGIT = 9;

    private static final char CELL_IMAGE = '#';
    private static final char MINE_IMAGE = 'X';
    private static final char FLAG_IMAGE = 'F';
    private static final char QUESTION_IMAGE = '?';

    public FieldTextView(int fieldWidth, int fieldHeight, FieldTextController fieldController, Field fieldModel)
    {
        assert (null != fieldController);
        assert (null != fieldModel);
        assert (0 < fieldHeight);
        assert (0 < fieldWidth);

        this.fieldWidth = fieldWidth;
        this.fieldHeight = fieldHeight;
        this.fieldModel = fieldModel;
        this.fieldController = fieldController;

        printField();
    }

    public void printField()
    {
        boolean drawMines = fieldController.isGameOver();

        for (int cellY = 0; fieldHeight > cellY; cellY++)
        {
            for (int cellX = 0; fieldWidth > cellX; cellX++)
            {
                char drawingImage = CELL_IMAGE;

                if (fieldModel.isOpened(cellX, cellY))
                {
                    int mineCount = fieldModel.countNeighbourMines(cellX, cellY);
                    drawingImage = Character.forDigit(mineCount, MAX_DIGIT);
                }
                else if (drawMines && (Cell.MINE == fieldModel.getCellContent(cellX, cellY)))
                {
                    drawingImage = MINE_IMAGE;
                }
                else if (fieldModel.isChecked(cellX, cellY))
                {
                    drawingImage = FLAG_IMAGE;
                }
                else if (fieldModel.isQuestioned(cellX, cellY))
                {
                    drawingImage = QUESTION_IMAGE;
                }

                System.out.print(drawingImage);
            }
            System.out.println();
        }
    }

    public void repaint()
    {
        int messageType = fieldController.processCommand();
        assert (MESSAGE_COUNT > messageType);

        if (FieldTextController.NOTHING == messageType)
        {
            printField();
        }
        else if (FieldTextController.NEW_FIELD != messageType)
        {
            printMessage(messageType);
        }
    }

    private void printMessage(int type)
    {
        System.out.println(MESSAGES[type]);
    }
}
