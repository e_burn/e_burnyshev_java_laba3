package minesweeper.userinterface.text.controller;

import minesweeper.model.field.Field;
import minesweeper.GameException.GameException;
import minesweeper.userinterface.text.TextGame;

import java.io.IOException;

public class FieldTextController
{
    public static final int NEW_FIELD = -2;
    public static final int NOTHING = -1;
    public static final int HELP = 0;
    public static final int FORMAT_ERROR = 1;
    public static final int DATA_ERROR = 2;

    public static final int MAX_FIELD_SIZE = 10;;

    private Field fieldModel;

    private boolean gameEnded = false;
    private boolean gameOver = false;

    public FieldTextController(Field fieldModel)
    {
        assert (null != fieldModel);
        this.fieldModel = fieldModel;
    }

    public int processCommand()
    {
        char command;

        try
        {
            command = readChar();

            if ('h' == command)
            {
                return HELP;
            }
            else if ('e' == command)
            {
                System.exit(0);
            }
            else if ('r' == command)
            {
                resetField();
            }
            else if ((('o' == command) || ('c' == command) || ('n' == command)) && (false == gameEnded))
            {
                int x = readInt();
                int y = readInt();

                if ('n' == command)
                {
                    return startNewGame(x, y);
                }
                else if ('o' == command)
                {
                    open(x, y);
                }
                else if ('c' == command)
                {
                    fieldModel.changeCheckStatus(x, y);
                }
                else
                {
                    return FORMAT_ERROR;
                }
            }
            else
            {
                return FORMAT_ERROR;
            }
        }
        catch (Exception exception)
        {
            return FORMAT_ERROR;
        }

        return NOTHING;
    }

    public void resetField()
    {
        fieldModel.reset();
        gameEnded = false;
        gameOver = false;
    }

    public int startNewGame(int x, int y) throws IOException
    {
        int mineCount = readInt();

        if ((x * y > mineCount) && (0 < x) && (0 < y) && (0 < mineCount) && (MAX_FIELD_SIZE > x) && (MAX_FIELD_SIZE > y))
        {
            TextGame.startGame(x, y, mineCount);
        }
        else
        {
            return DATA_ERROR;
        }

        return NEW_FIELD;
    }

    public void open(int x, int y)
    {
        try
        {
            fieldModel.changeOpenStatus(x, y);
        }
        catch (GameException e)
        {
            gameEnded = true;

            if (GameException.GAME_OVER == e.getType())
            {
                System.out.println("Game Over");
                gameOver = true;
            }
            else
            {
                System.out.println("You Win!");
            }
        }
    }

    public boolean isGameOver()
    {
        return gameOver;
    }

    public static int readInt() throws IOException, NumberFormatException
    {
        final int EOF = -1;
        int returnValue;
        StringBuilder stringResult = new StringBuilder();

        byte [] readBuffer = new byte[1];

        for(;;)
        {
            returnValue = System.in.read(readBuffer);
            if (EOF == returnValue)
            {
                throw new NumberFormatException();
            }

            if (Character.isDigit((char) readBuffer[0]))
            {
                stringResult.append((char) readBuffer[0]);
                break;
            }
        }

        for(;;)
        {
            returnValue = System.in.read(readBuffer);
            if (EOF == returnValue)
            {
                break;
            }

            if (false == Character.isDigit((char) readBuffer[0]))
            {
                break;
            }

            stringResult.append((char) readBuffer[0]);
        }

        return Integer.parseInt(stringResult.toString());
    }

    public static char readChar() throws IOException
    {
        final int EOF = -1;
        int returnValue;
        byte [] readBuffer = new byte[1];

        do
        {
            returnValue = System.in.read(readBuffer);
            if (EOF == returnValue)
            {
                throw new IOException();
            }
        }
        while (false == Character.isLetter((char) readBuffer[0]));

        return (char) readBuffer[0];
    }
}
