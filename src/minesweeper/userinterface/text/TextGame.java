package minesweeper.userinterface.text;

import minesweeper.model.field.Field;
import minesweeper.userinterface.text.controller.FieldTextController;

public final class TextGame
{
    private static FieldTextView fieldView;

    private static final int INITIAL_WIDTH = 9;
    private static final int INITIAL_HEIGHT = 9;
    private static final int INITIAL_MINE_COUNT = 10;

    public TextGame()
    {
        startGame(INITIAL_WIDTH, INITIAL_HEIGHT, INITIAL_MINE_COUNT);
        for(;;)
        {
            fieldView.repaint();
        }
    }

    public static void startGame(int fieldWidth, int fieldHeight, int mineCount)
    {
        Field fieldModel = new Field(fieldWidth, fieldHeight, mineCount);
        FieldTextController fieldController = new FieldTextController(fieldModel);
        fieldView = new FieldTextView(fieldWidth, fieldHeight, fieldController, fieldModel);
    }
}
