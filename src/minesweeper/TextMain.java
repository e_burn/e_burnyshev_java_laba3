package minesweeper;

import minesweeper.userinterface.text.TextGame;

public final class TextMain
{
    public static void main(String args[])
    {
        try
        {
            new TextGame();
        }
        catch (Throwable exception)
        {
            System.out.println("Unknown error was found.");
            exception.printStackTrace();
        }
    }
}
